/* Abstract Adjacency List Graphs */

#pragma once

#include <set>
#include <istream>
#include <ostream>
#include <locale>
#include <stdexcept>
#include <iterator>

namespace vabor112 {
	
	// Some exception classes for convinience.
	class NoSuchEdgeException: public std::runtime_error {
		public:
			NoSuchEdgeException() : std::runtime_error("There is no such edge in the graph.") {}
	};

	class NoSuchVertexException: public std::runtime_error {
		public:
			NoSuchVertexException() : std::runtime_error("There is no such vertex in the graph.") {}
			NoSuchVertexException(const std::string& msg) : std::runtime_error(msg) {}
	};

	//********************** Base adjacency list graph ************************

	template <class Vertex> class ALGraph {
		public:
			// Returns the current number of vertexes in graph. Subclasses are
			// allowed to be either dynamic or static i.e. this value can be
			// stable through graph lifetime or change spontaneously in time
			// depending on particular implementation.
			virtual size_t get_number_of_vertexes() const = 0;

			// Returns adjacency list corresponding to vertex'th vertex. The
			// value is returned by constant reference. This "format" is chosen
			// because of graphs often being big considerations. Subclasses may
			// allow direct access to adjacency list.
			//
			// If vertex>number_of_vertexes should throw NoSuchVertexException.
			virtual std::set<Vertex> adj_list(size_t vertex) const = 0;

			// Returns if the graph has edge between from and to vertexes. No
			// additional information(like weight) provided.
			virtual bool has_edge(size_t from, size_t to) const = 0;

			// Returns the "vertex" of the graph with additional information
			// i.e. weight. If there is no edge between from and to should
			// throw NoSuchEdgeException.
			virtual Vertex get_edge(size_t from, size_t to) const = 0;

			virtual ~ALGraph() {};
			
			// Requires std::ostream& operator<<(std::ostream&, const Vertex&)
			// operator overloaded.
			virtual std::ostream& output(std::ostream& out) const{
				size_t number_of_vertexes = get_number_of_vertexes();
				out << number_of_vertexes << std::endl;
				for(size_t i = 0; i < number_of_vertexes; i++) {
					for(typename std::set<Vertex>::iterator it = adj_list(i).begin(); it != adj_list(i).end(); it++) {
						out << *it;
						if(std::next(it) != adj_list(i).end())
							out << ' ';
					}
					if(i + 1 < number_of_vertexes)
						out << '\n';
				}
				return out;
			}

			// More elegant adj_list().
			const std::set<Vertex>& operator[] (size_t vertex) {
				return adj_list(vertex);
			}

	};

	template <class Vertex>
	std::ostream& operator<<(std::ostream& out, const ALGraph<Vertex>& g) { 
		return g.output(out);
	}

	//******************** Weighted adjacency list graph **********************
	
	template <class Weight> struct WeightedVertex {
		size_t vertex;
		Weight weight;

		WeightedVertex(size_t _vertex) {
			vertex = _vertex;
		}

		WeightedVertex(size_t _vertex, Weight _weight) {
			vertex = _vertex;
			weight = _weight;
		}

		WeightedVertex() {}
	};

	template <class Weight> bool operator<(WeightedVertex<Weight> lhs, WeightedVertex<Weight> rhs) {
		return lhs.vertex < rhs.vertex;
	}

	template <class Weight> bool operator==(WeightedVertex<Weight> lhs, WeightedVertex<Weight> rhs) {
		return lhs.vertex == rhs.vertex;
	}

	template <class Weight> std::ostream& operator<<(std::ostream& output, const WeightedVertex<Weight>& vertex) { 
		output << '(' << vertex.vertex << ',' << vertex.weight << ')';
		return output;
	}
	
	// This will make ',' a delimiter.
	struct weighted_vertex_delimiters : std::ctype<char> {
		weighted_vertex_delimiters() : std::ctype<char>(get_table()) {}
		static mask const* get_table() {
			static mask rc[table_size];
			rc[' '] = std::ctype_base::space;
			rc[','] = std::ctype_base::space;
			rc['\n'] = std::ctype_base::space;
			return &rc[0];
		}
	};

	template <class Weight> std::istream& operator>>(std::istream& input, WeightedVertex<Weight>& vertex) {
		std::locale input_locale = input.getloc();
		input.imbue(std::locale(input_locale, new weighted_vertex_delimiters()));

		input.ignore(1);
		input >> vertex.vertex;
		input >> vertex.weight;
		input.ignore(1);

		input.imbue(input_locale);
		return input;
	}

	template <class Weight> class WeightedALGraph : public ALGraph<WeightedVertex<Weight>> {};



	//******************* Unweighted adjacency list graph *********************
	
	class UnweightedALGraph : public ALGraph<size_t> {};
}
