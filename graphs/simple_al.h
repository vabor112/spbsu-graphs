/* Simple Adjacency List Graph realization */

#pragma once

#include <vector>
#include <set>
#include <istream>
#include <ostream>
#include <locale>
#include <iostream>

#include "al.h"

namespace vabor112 {
	template <class Weight> class SimpleWeightedALGraph : public WeightedALGraph<Weight> {

		private:
			typedef std::set<WeightedVertex<Weight> > AdjListType;
			std::vector<AdjListType>* adj_lists;

		public:
			SimpleWeightedALGraph() {
				adj_lists = new std::vector<AdjListType>();
			}

			virtual ~SimpleWeightedALGraph() {
				delete adj_lists;
			}

			virtual size_t get_number_of_vertexes() const {
				return adj_lists -> size();
			}

			virtual const AdjListType& adj_list(size_t vertex) const {
				if(vertex < adj_lists -> size())
					return adj_lists -> at(vertex);
				else
					throw NoSuchVertexException();
			}

			virtual bool has_edge(size_t from, size_t to) const {
				if((from >= adj_lists -> size()) || (to >= adj_lists -> size()))
					return false;
				AdjListType& adj_list = adj_lists -> at(from);
				WeightedVertex<Weight> search_pattern(to);
				return (adj_list.find(search_pattern) != adj_list.end());
			}

			virtual WeightedVertex<Weight> get_edge(size_t from, size_t to) const {
				if((from >= adj_lists -> size()) || (to >= adj_lists -> size()))
					throw NoSuchVertexException();
				AdjListType& adj_list = adj_lists -> at(from);
				WeightedVertex<Weight> search_pattern(to);
				auto it = adj_list.find(search_pattern);
				if(it != adj_list.end())
					return *it;
				else
					throw NoSuchEdgeException();
			}

			// W is for Weight(which is already taken).
			template <class W> friend std::istream& operator>>(std::istream& input, SimpleWeightedALGraph<W>& g);
	};

	template <class Weight> std::istream& operator>>(std::istream& input, SimpleWeightedALGraph<Weight>& g) {
		size_t number_of_vertexes;
		input >> number_of_vertexes;
		input.ignore(1); // skip '\n'
		g.adj_lists -> resize(number_of_vertexes);

		for(size_t i = 0; i < number_of_vertexes; i++) {
			while(input.good() && (input.peek() != '\n')) {
				WeightedVertex<Weight> vertex;
				input >> vertex;
				(g.adj_lists -> at(i)).insert(vertex);
				if(input.peek() == ' ')
					input.ignore(1);
			}
			// Skip '\n'.
			input.ignore(1);
		}

		return input;
	}

}
