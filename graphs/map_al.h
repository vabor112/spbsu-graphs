/* Simple Adjacency List Graph realization */

#pragma once

#include <vector>
#include <set>
#include <istream>
#include <ostream>
#include <locale>
#include <iostream>

#include "al.h"

namespace vabor112 {
	class MapAL : public WeightedALGraph<int> {
		private:
			int width, height;
			std::set<int>* barriers;

		public:
			MapAL(int _width, int _height) {
				width = _width;
				height = _height;
				barriers = new std::set<int>();
			}

			virtual ~MapAL() {
				delete barriers;
			}

			virtual size_t get_number_of_vertexes() const {
				return (width * height);
			}

			inline bool lies_on_map(int x_coord, int y_coord) const {
				return ((x_coord >= 0) && (y_coord >= 0) && (x_coord < width) && (y_coord < height));
			}

			int get_vertex(int x_coord, int y_coord) const {
				if(!lies_on_map(x_coord, y_coord))
					throw NoSuchVertexException("The point is outside of map!");
				else
					return ((y_coord * width) + x_coord);
			}

			std::pair<int, int> get_point(int vertex) const {
				return std::pair<int, int>(vertex % width, vertex / width);
			}

			inline bool is_barrier(int vertex) const {
				return (barriers -> find(vertex) != barriers -> end());
			}

			inline bool is_barrier(int x_coord, int y_coord) const {
				return is_barrier(get_vertex(x_coord, y_coord));
			}

			void add_barrier(int vertex) {
				barriers -> insert(vertex);
			}

			void add_barrier(int x_coord, int y_coord) {
				add_barrier(get_vertex(x_coord, y_coord));
			}

			const std::set<int>& get_barriers() const {
				return *barriers;
			}

			virtual std::set<WeightedVertex<int>> adj_list(size_t vertex) const {
				std::set<WeightedVertex<int>> result;

				int x_coord = vertex % width;
				int y_coord = vertex / width;

				for(int i = -1; i <= 1; i++) {
					for(int j = -1; j <= 1; j++) {
						int cur_x = x_coord + i;
						int cur_y = y_coord + j;
						if(((i != 0) || (j != 0)) && lies_on_map(cur_x, cur_y) && !is_barrier(cur_x, cur_y)) {
							if(abs(i*j) == 1)
								result.insert(WeightedVertex<int>(get_vertex(cur_x, cur_y), 14));
							else
								result.insert(WeightedVertex<int>(get_vertex(cur_x, cur_y), 10));
						}
					}
				}

				return result;
			}

			virtual bool has_edge(size_t from, size_t to) const {
				int x_coord_from = from % width;
				int y_coord_from = from / width;

				int x_coord_to = to % width;
				int y_coord_to = to / width;

				return ((abs(x_coord_from - x_coord_to) <= 1) && (abs(y_coord_from - y_coord_to) <= 1));
			}

			virtual WeightedVertex<int> get_edge(size_t from, size_t to) const {
				int x_coord_from = from % width;
				int y_coord_from = from / width;

				int x_coord_to = to % width;
				int y_coord_to = to / width;

				int x_diff = x_coord_from - x_coord_to;
				int y_diff = y_coord_from - y_coord_to;

				if(abs(x_diff * y_diff) == 1)
					return WeightedVertex<int>(to, 14);
				else
					return WeightedVertex<int>(to, 10);
			}
	};
}
