#pragma once

const char* WINDOW_TITLE = "Path-finding visualisation";

const int GRID_WIDTH = 25;
const int GRID_HEIGHT = 25;

const int CELL_WIDTH = 27;
const int CELL_HEIGHT = 27;

const int GRID_BORDER = 1;

const int WINDOW_WIDTH = GRID_WIDTH * CELL_WIDTH + (GRID_WIDTH - 1) * GRID_BORDER;
const int WINDOW_HEIGHT = GRID_HEIGHT * CELL_HEIGHT + (GRID_HEIGHT - 1) * GRID_BORDER;

const int update_interval = 10;

struct rgba {Uint8 r,g,b,a;};

// Colors:

const rgba BACKGROUND_COLOR = {255, 255, 255, 255}; // White
const rgba BORDERS_COLOR = {0, 0, 0, 255}; // Black
const rgba BARRIERS_COLOR = {157, 157, 157, 200}; // Gray
const rgba BEING_PROCESSED_COLOR = {255, 255, 200, 200}; // Light yellow
const rgba PROCESSED_COLOR = {255, 255, 0, 200}; // Bright yellow
const rgba PATH_COLOR = {0, 150, 0, 200}; // Green
const rgba START_COLOR = {3, 244, 0, 250}; // Bright green
const rgba FINISH_COLOR = {255, 68, 51, 250}; // Red
const rgba CURSOR_COLOR = {153, 204, 255, 100}; // Light blue

