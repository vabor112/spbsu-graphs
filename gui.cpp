#include "SDL.h"
#include "SDL_gfxPrimitives.h"
#include "SDL_rotozoom.h"
#include <vector>
#include <iostream>
#include <set>
#include <queue>
#include <map>
#include <utility>
#include <thread>
#include <mutex>
#include <cmath>

#include "config.h"
#include "graphs/map_al.h"
#include "p_queue/p_queue.h"
#include "algorithm/a_star.h"

// Euler distance metric.
int e_dist(const std::pair<int,int>& x, const std::pair<int,int>& y) {
	double dist = (sqrt((x.first-y.first)*(x.first-y.first)
				+ (x.second - y.second)*(x.second - y.second)));
	dist *= 10;
	return (int) dist;
}

inline void drawCell(SDL_Surface* screen, const std::pair<int,int>& point,
		const rgba& color) {
	int cell_left = point.first * (CELL_WIDTH + GRID_BORDER) + GRID_BORDER;
	int cell_top = point.second * (CELL_HEIGHT + GRID_BORDER) + GRID_BORDER;
	int cell_right = cell_left + CELL_WIDTH;
	int cell_bottom = cell_top + CELL_HEIGHT;

	boxRGBA(screen, cell_left, cell_top, cell_right, cell_bottom,
			color.r, color.g, color.b, color.a);
}

inline void drawCell(SDL_Surface* screen, const vabor112::MapAL& map, int vertex,
		const rgba& color) {
	std::pair<int, int> point = map.get_point(vertex);
	drawCell(screen, point, color);
}

int main(int argc, char **argv)
{
	SDL_Init( SDL_INIT_VIDEO );

	SDL_Surface* screen = SDL_SetVideoMode( WINDOW_WIDTH, WINDOW_HEIGHT, 0, 
			SDL_HWSURFACE | SDL_DOUBLEBUF );
	SDL_WM_SetCaption( WINDOW_TITLE, 0 );

	SDL_Event event;

	bool running = true;
	bool search_running = false;

	std::mutex mtx;

	int start = -1;
	int finish = -1;

	vabor112::MapAL map(GRID_WIDTH, GRID_HEIGHT);
	vabor112::AStarPriorityQueue<int> being_processed;
	std::set<size_t> processed;
	std::map<size_t, size_t> ancestors;

	while(running)
	{
		int mouse_x, mouse_y;
		SDL_GetMouseState(&mouse_x, &mouse_y);

		// Find cell coords
		int cell_x = (mouse_x - GRID_BORDER) / (CELL_WIDTH + GRID_BORDER);
		int cell_y = (mouse_y - GRID_BORDER) / (CELL_HEIGHT + GRID_BORDER);
		cell_x = cell_x >= 0 ? cell_x : 0;
		cell_y = cell_y >= 0 ? cell_y : 0;
		
		//std::cout << "Cell: (" << cell_x << ", " << cell_y << ")" << std::endl;

		if(SDL_PollEvent(&event)) {
			if(event.type == SDL_QUIT) {
				running = false;
			} else if(event.type == SDL_KEYDOWN) {
				// 's' key for start point.
				// 'f' key for finish point.
				// 'b' key for bfs start.
				// 'a' key for A* start.
				// Mouse motion for borders.
				if((event.key.keysym.sym == SDLK_s)
						&& (map.get_vertex(cell_x, cell_y) != finish)){
					start = map.get_vertex(cell_x, cell_y);
					//start.first = cell_x; start.second = cell_y;
				} else if ((event.key.keysym.sym == SDLK_f)
						&& (map.get_vertex(cell_x, cell_y) != start)) {
					finish = map.get_vertex(cell_x, cell_y);
					//finish.first = cell_x; finish.second = cell_y;
				} else if (((event.key.keysym.sym == SDLK_b)
						|| (event.key.keysym.sym == SDLK_a)) 
						&& (start > 0) && (finish > 0)) {
					std::function<int(size_t,size_t)> dist;
					if(event.key.keysym.sym == SDLK_b)
						dist = [](size_t x, size_t y){return 0;};
					else
						dist = [&map](size_t x, size_t y){return e_dist(map.get_point(x), map.get_point(y));};
					std::thread search_thread(vabor112::a_star_visual<int>, std::ref(ancestors),
							std::ref(processed), std::ref(being_processed),
							std::ref(mtx), std::cref(map), start, finish, dist, 0, 10);
					search_thread.detach();
				}
			} else if(event.type == SDL_MOUSEMOTION) {
				if(event.motion.state == SDL_BUTTON_LMASK)
					map.add_barrier(cell_x, cell_y);
			}

		}

		// Draw background:
		SDL_FillRect(screen, NULL, SDL_MapRGB(screen->format, 255, 255, 255));

		// Draw borders:
		for(int i = 0; i < GRID_WIDTH; i++)
			boxRGBA(screen, i*(CELL_WIDTH + GRID_BORDER), 0,
					i*(CELL_WIDTH + GRID_BORDER) + GRID_BORDER, WINDOW_HEIGHT,
					0, 0, 0, 255);
		for(int i = 0; i < GRID_WIDTH; i++)
			boxRGBA(screen, 0, i*(CELL_HEIGHT + GRID_BORDER),
					WINDOW_WIDTH, i*(CELL_HEIGHT + GRID_BORDER) + GRID_BORDER,
					0, 0, 0, 255);

		// Draw start and finish points:
		if(start > 0)
			drawCell(screen, map, start, START_COLOR);
		if(finish > 0)
			drawCell(screen, map, finish, FINISH_COLOR);

		// Draw barriers:
		for(auto it = map.get_barriers().begin(); it != map.get_barriers().end(); it++)
			drawCell(screen, map, *it, BARRIERS_COLOR);

		mtx.lock();

		// Draw points that are being processed:
		for(auto it = being_processed.begin(); it != being_processed.end(); it++)
			drawCell(screen, map, it -> value(), BEING_PROCESSED_COLOR);

		// Draw processed points:
		for(auto it = processed.begin(); it != processed.end(); it++)
			drawCell(screen, map, *it, PROCESSED_COLOR);

		// Draw path:
		auto path_it = ancestors.find(finish);
		while((path_it != ancestors.end()) && ((path_it -> second) != start)) {
			drawCell(screen, map, path_it -> second, PATH_COLOR);
			path_it = ancestors.find(path_it -> second);
		}

		mtx.unlock();

		// Draw cursor:
		drawCell(screen, std::pair<int,int>(cell_x, cell_y), CURSOR_COLOR);

		SDL_Flip(screen);
		sleep(0);
	}
	SDL_Quit();

	return 0;
}
