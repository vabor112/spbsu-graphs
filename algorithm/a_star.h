#pragma once

#include <functional>
#include <chrono>
#include <mutex>

#include "../graphs/al.h"
#include "../graphs/simple_al.h"
#include "../p_queue/p_queue.h"

namespace vabor112 {

//************************** Some priority queue stuff ************************

	template <class Weight> struct Path {
		size_t vertex;
		Weight weight;
		Weight estimate;	

		Path(size_t _vertex, Weight _weight, Weight _estimate) {
			vertex = _vertex;
			weight = _weight;
			estimate = _estimate;
		}

		size_t value() const {
			return vertex;
		}
	};

	template <class Weight> inline bool operator<(const Path<Weight>& lhs, const Path<Weight>& rhs) {
		return (lhs.weight + lhs.estimate) < (rhs.weight + rhs.estimate);
	}

	template <class Weight> inline bool operator>(const Path<Weight>& lhs, const Path<Weight>& rhs) {
		return (lhs.weight + lhs.estimate) > (rhs.weight + rhs.estimate);
	}

	class NoSuchVertexInHeapException: public std::runtime_error {
		public:
			NoSuchVertexInHeapException() : std::runtime_error("There is no such vertex in the heap!") {}
	};

	template <class Weight> class AStarPriorityQueue : public LinkedPriorityQueue<Path<Weight>, size_t> {
		public:
			Weight weight(size_t vertex) {
				int pointer = this -> find(vertex);
				if(pointer >= 0)
					return ((*this)[pointer]).weight;
				else
					throw NoSuchVertexInHeapException();
			}

			Weight estimate(size_t vertex) {
				int pointer = this -> find(vertex);
				if(pointer >= 0)
					return ((*this)[pointer]).estimate;
				else
					throw NoSuchVertexInHeapException();
			}
	};
	
//**************************** The algorithm itself ***************************
	
	// A* search algorythm. Searches for path from start vertex to finish. Writes
	// path to the ancestors vector. If there is no path between start and finish
	// map will be empty.
	template <class Weight>
	void a_star(std::map<size_t, size_t>& ancestors,
			const WeightedALGraph<Weight>& g, size_t start, size_t finish,
			std::function<Weight(size_t,size_t)> dist, const Weight ZERO) {
		ancestors.clear();
		std::set<size_t> processed;
		AStarPriorityQueue<Weight> being_processed;

		being_processed.insert(Path<Weight>(start, ZERO, dist(start, finish)));

		while(!being_processed.empty()) {
			Path<Weight> cur_path = being_processed.extract_min();
			size_t cur_vertex = cur_path.vertex;
			Weight cur_weight = cur_path.weight;
			
			if(cur_vertex == finish)
				break;

			for(const WeightedVertex<Weight>& neighbour : g.adj_list(cur_vertex)) {
				if(processed.find(neighbour.vertex) == processed.end()) {
					int pointer = being_processed.find(neighbour.vertex);
					if(pointer >= 0) {
						if(cur_weight + neighbour.weight < being_processed.weight(neighbour.vertex)) {
							being_processed.decrease_key(pointer, Path<Weight>(neighbour.vertex, neighbour.weight + cur_weight, being_processed.estimate(neighbour.vertex)));
							ancestors[neighbour.vertex] = cur_vertex;
						}
					} else {
						being_processed.insert(Path<Weight>(neighbour.vertex, cur_weight + neighbour.weight, dist(neighbour.vertex, finish)));
						ancestors[neighbour.vertex] = cur_vertex;
					}
				}
			}
			processed.insert(cur_vertex);
		}
	}

	// Same shit. Visualization-optimized.
	template <class Weight>
	void a_star_visual(std::map<size_t, size_t>& ancestors, std::set<size_t>& processed,
			AStarPriorityQueue<Weight>& being_processed, std::mutex& mtx, const WeightedALGraph<Weight>& g,
			size_t start, size_t finish, std::function<Weight(size_t,size_t)> dist, const Weight ZERO,
			int update_interval) {
		ancestors.clear();
		processed.clear();
		being_processed.clear();
		being_processed.insert(Path<Weight>(start, ZERO, dist(start, finish)));

		while(!being_processed.empty()) {
			mtx.lock();
			Path<Weight> cur_path = being_processed.extract_min();
			size_t cur_vertex = cur_path.vertex;
			Weight cur_weight = cur_path.weight;
			
			if(cur_vertex == finish) {
				mtx.unlock();
				break;
			}

			for(const WeightedVertex<Weight>& neighbour : g.adj_list(cur_vertex)) {
				if(processed.find(neighbour.vertex) == processed.end()) {
					int pointer = being_processed.find(neighbour.vertex);
					if(pointer >= 0) {
						std::cout << "vertex (" << neighbour.vertex % 25 << ", " << neighbour.vertex / 25 << ") FOUND" << std::endl;
						if(cur_weight + neighbour.weight < being_processed.weight(neighbour.vertex)) {
							being_processed.decrease_key(pointer, Path<Weight>(neighbour.vertex, neighbour.weight + cur_weight, being_processed.estimate(neighbour.vertex)));
							ancestors[neighbour.vertex] = cur_vertex;
							std::cout << "Key decreased!" << std::endl;
							std::cout << "ancestors[" << neighbour.vertex % 25 << ", " << neighbour.vertex / 25 << "] = (" << cur_vertex % 25 << ", " << cur_vertex / 25 << ")" << std::endl;
						}
						else
							std::cout << "vertex (" << neighbour.vertex % 25 << ", " << neighbour.vertex / 25 << ") not less" << std::endl;

					} else {
						being_processed.insert(Path<Weight>(neighbour.vertex, cur_weight + neighbour.weight, dist(neighbour.vertex, finish)));
						ancestors[neighbour.vertex] = cur_vertex;
							std::cout << "ancestors[" << neighbour.vertex % 25 << ", " << neighbour.vertex / 25 << "] = (" << cur_vertex % 25 << ", " << cur_vertex / 25 << ")" << std::endl;
					}
				}
			}
			processed.insert(cur_vertex);
			mtx.unlock();
			if(update_interval > 0)
				std::this_thread::sleep_for(std::chrono::milliseconds(update_interval));
		}
	}
}
