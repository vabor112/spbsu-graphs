/* Priority queue realisation. Based on heaps. */

#pragma once

#include <cstring>
#include <vector>
#include <ostream>
#include <map>

namespace vabor112
{
	class TheHeapIsEmptyException: public std::runtime_error {
		public:
			TheHeapIsEmptyException() : std::runtime_error("The heap is empty!") {}
	};

	class WrongValueException: public std::runtime_error {
		public:
			WrongValueException() : std::runtime_error("You can't change value via decrease_key!") {}
	};

	class WrongKeyException: public std::runtime_error {
		public:
			WrongKeyException() : std::runtime_error("The key can't be larger than current one!") {}
	};

	template <class T, class Value> class LinkedPriorityQueue {
		private:
			std::vector<T>* heap;
			std::map<Value, size_t>* pointers;
			
			// Because 0 is the first element.
			inline size_t parent(size_t el)
			{ return (el-1)/2; }

			inline size_t left(size_t el)
			{ return 2*el + 1; }

			inline size_t right(size_t el)
			{ return 2*el + 2; }

			void min_heapify(size_t pointer) {
				size_t min;
				if((left(pointer) < heap -> size()) && (heap -> at(pointer) > heap -> at(left(pointer))))
					min = left(pointer);
				else
					min = pointer;
				if((right(pointer) < heap -> size()) && (heap -> at(min) > heap -> at(right(pointer))))
					min = right(pointer);
				
				if(min != pointer)
				{
					T temp = heap -> at(pointer);
					heap -> at(pointer) = heap -> at(min);
					heap -> at(min) = temp;
					(*pointers)[(heap -> at(pointer)).value()] = pointer;
					(*pointers)[temp.value()] = min;
					min_heapify(min);
				}
			}

		public:
			typedef typename std::vector<T>::iterator iterator;
			typedef typename std::vector<T>::const_iterator const_iterator;

			iterator begin() { return heap -> begin(); }
			iterator end() { return heap -> end(); }

			const_iterator begin() const { return heap -> begin(); }
			const_iterator end() const { return heap -> end(); }

			LinkedPriorityQueue() {
				heap = new std::vector<T>();
				pointers = new std::map<Value, size_t>();
			}

			~LinkedPriorityQueue() {
				delete heap;
				delete pointers;
			}

			void insert(T el) {
				heap -> push_back(el);
				(*pointers)[el.value()] = heap -> size() - 1;
				decrease_key(heap -> size() - 1, el);
			}
			
			void decrease_key(size_t pointer, T el) {
				if(el > heap -> at(pointer))
					throw WrongKeyException();
				if((*pointers)[el.value()] != pointer)
					throw WrongValueException();
				heap -> at(pointer) = el;
				while((pointer > 0) && (heap -> at(parent(pointer)) > heap -> at(pointer))) {
					T temp = heap -> at(pointer);
					heap -> at(pointer) = heap -> at(parent(pointer));
					heap -> at(parent(pointer)) = temp;
					(*pointers)[(heap -> at(pointer)).value()] = pointer;
					(*pointers)[temp.value()] = parent(pointer);
					pointer = parent(pointer);
				}
			}

			T min() const {
				if((heap -> size()) > 0)
					return heap -> at(0);
				else
					throw TheHeapIsEmptyException();
			}

			T extract_min() {
				if((heap -> size()) > 0) {
					T min = heap -> at(0);
					heap -> front() = heap -> back();
					heap -> pop_back();
					(*pointers).erase(min.value());
					if(!heap -> empty())
						(*pointers)[(heap -> front()).value()] = 0;
					min_heapify(0);
					return min;
				} else
					throw TheHeapIsEmptyException();
			}

			int find(Value value) const {
				auto it = pointers -> find(value);
				if(it == pointers -> end())
					return -1;
				else
					return it -> second;
			}

			inline T operator[](int pointer) const {
				return heap -> at(pointer);
			}

			bool empty() const {
				return heap -> empty();
			}

			void clear() {
				heap -> clear();
				pointers -> clear();
			}
	};
}
